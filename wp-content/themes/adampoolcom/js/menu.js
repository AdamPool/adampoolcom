$('.site-header .main-section .main-navigation label input').change(function() {
    if ($(this).is(":checked")) {
        $(".site-header .main-section .main-navigation .outer-menu").addClass("open");
    } else {
        $(".site-header .main-section .main-navigation .outer-menu").removeClass("open");
    }
});

$('#primary-menu > li > a').click(function() {
    $('.site-header .main-section .main-navigation label input').click();
});


$(window).on('scroll', function() {
    var scrollTop = $(window).scrollTop()

    var home = $('#main').offset().top;
    var about_top = $('#about > .top').offset().top - 52;
    var about_bottom = $('#about > .bottom').offset().top - 52;
    var live_projects = $('#live-projects').offset().top - 52;
    var contact = $('#contact').offset().top - 52;

    if (scrollTop > home && scrollTop < about_top || scrollTop > about_bottom && scrollTop < contact) { 
      $('.site-header .main-section .main-navigation .outer-menu>div .menu-main-menu-container .menu .menu-item a').css('color', '#424242');
    }

    if (scrollTop > about_top && scrollTop < about_bottom || scrollTop > contact) {
        $('.site-header .main-section .main-navigation .outer-menu>div .menu-main-menu-container .menu .menu-item a').css('color', '#ffffff');
    }
});