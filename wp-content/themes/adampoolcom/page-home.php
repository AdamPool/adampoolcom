<?php

/**
 * Template Name: Home Page
 */
?>

<?php get_header(); ?>

<section id="main">

    <div class="container">
        <div class="headshot">
            <img src="/wp-content/uploads/2022/04/headshot.webp" alt="adam pool headshot">
        </div>
        <h1><span>Designer,</span><span>Developer,</span><span>Problem Solver</span></h1>
    </div>


    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 375 375">
        <path d="M375,374.93c-50.12-12.11-100.3-24.23-133.56-52.45s-49.8-72.4-87.36-101.56S57.82,177.81,28.6,143.47-.25,54.6.07,0H375Z" />
    </svg>

    <svg viewBox="0 0 375 375">
        <path d="M0,0C50.57,9.72,101.07,19.44,136.24,46s54.92,70.14,91.85,100.89,91,48.67,118.37,84.59S374.37,321.21,375,375H0Z"/>
    </svg>
</section>

<section id="about">
    <div class="top">
        <div class="container">
            <h2>Hey! My name's Adam</h2>
            <p>I’m a hard working and enthusiastic Website Developer, Graphic Designer and all round problem solver that loves a challenge and is passionate about the ever evolving face of technology in our world.</p>
        </div>
    </div>
    <div class="bottom">
        <div class="container">

        <div class="about-inner">
            <div class="card" id="languages">
                <div class="icon">
                    <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#FFFFFF"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M12.87 15.07l-2.54-2.51.03-.03c1.74-1.94 2.98-4.17 3.71-6.53H17V4h-7V2H8v2H1v1.99h11.17C11.5 7.92 10.44 9.75 9 11.35 8.07 10.32 7.3 9.19 6.69 8h-2c.73 1.63 1.73 3.17 2.98 4.56l-5.09 5.02L4 19l5-5 3.11 3.11.76-2.04zM18.5 10h-2L12 22h2l1.12-3h4.75L21 22h2l-4.5-12zm-2.62 7l1.62-4.33L19.12 17h-3.24z"/></svg>
                </div>
                <h3>Languages</h3>
                <p>CSS3, Git, HTML5, JavaScript, jQuery, MySQL, PHP, Sass</p>
            </div>

            <div class="card" id="tools">
                <div class="icon">
                    <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24px" viewBox="0 0 24 24" width="24px" fill="#FFFFFF"><g><rect fill="none" height="24" width="24"/></g><g><g><rect height="8.48" transform="matrix(0.7071 -0.7071 0.7071 0.7071 -6.8717 17.6255)" width="3" x="16.34" y="12.87"/><path d="M17.5,10c1.93,0,3.5-1.57,3.5-3.5c0-0.58-0.16-1.12-0.41-1.6l-2.7,2.7L16.4,6.11l2.7-2.7C18.62,3.16,18.08,3,17.5,3 C15.57,3,14,4.57,14,6.5c0,0.41,0.08,0.8,0.21,1.16l-1.85,1.85l-1.78-1.78l0.71-0.71L9.88,5.61L12,3.49 c-1.17-1.17-3.07-1.17-4.24,0L4.22,7.03l1.41,1.41H2.81L2.1,9.15l3.54,3.54l0.71-0.71V9.15l1.41,1.41l0.71-0.71l1.78,1.78 l-7.41,7.41l2.12,2.12L16.34,9.79C16.7,9.92,17.09,10,17.5,10z"/></g></g></svg>
                </div>
                <h3>Tools</h3>
                <p>Adobe Creative Suite, Bootstrap, Github/Gitlab, Google Analytics, Gulp, Hotjar, NPM, Terminal/Powershell, Underscores, Visual Studio Code/Atom, Wordpress, XAMPP/MAMP</p>
            </div>

            <div class="card" id="experience">
                <div class="icon">
                    <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#FFFFFF"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M20 6h-4V4c0-1.11-.89-2-2-2h-4c-1.11 0-2 .89-2 2v2H4c-1.11 0-1.99.89-1.99 2L2 19c0 1.11.89 2 2 2h16c1.11 0 2-.89 2-2V8c0-1.11-.89-2-2-2zm-6 0h-4V4h4v2z"/></svg>
                </div>
                <h3>Experience</h3>
                


                <div class="job">
                    <span class="company">Senior Web Developer</span>
                    <span class="time">
                        <span>Transalis Ltd</span>
                        <span>1 Year, 11 Months</span>
                    </span>
                </div>

                <div class="job">
                    <span class="company">Front End Web Developer</span>
                    <span class="time">
                        <span>Transalis Ltd</span>
                        <span>4 Years, 4 Months</span>
                    </span>
                </div>

                <div class="job">
                    <span class="company">Web Designer & Developer</span>
                    <span class="time">
                        <span>Accelerator</span>
                        <span>1 Year, 3 Months</span>
                    </span>
                </div>

                <div class="job">
                    <span class="company">Web Designer & Developer</span>
                    <span class="time">
                        <span>Adam Pool Design</span>
                        <span>3 Years, 5 Months</span>
                    </span>
                </div>
            </div>
        </div>

        </div>
    </div>
</section>

<section id="live-projects">
    <div class="container">
        <h2>Live Projects</h2>
        <p>Here’s a selection of websites I’ve worked on recently. <a href="mailto:hello@adampool.com">Give me a shout</a> if you want to see more.</p>

        <div class="projects">
            <a href="https://www.transalis.com/" target="_blank" class="card" id="transalis" aria-label="Transalis">
                <img src="/wp-content/uploads/2022/03/Transalis_logo_white.svg" alt="">
            </a>
            <a href="https://argosasist.co.uk/" target="_blank" class="card" id="argos" aria-label="Argos">
                <img src="/wp-content/uploads/2022/03/Argos_logo.svg" alt="">
            </a>
            <a href="https://www.fishscience.co.uk" target="_blank" class="card" id="fishscience" aria-label="FishScience">
                <img src="/wp-content/uploads/2022/03/fishscience-white.svg" alt="">
            </a>
            <a href="https://open.openedi.com/" target="_blank" class="card" id="openedi" aria-label="OpenEDI">
                <img src="/wp-content/uploads/2022/03/OpenEDI_-_White-01.svg" alt="">
            </a>
        </div>
    </div>
</section>

<section id="contact">
    <div class="container">
        <div class="content">
            <h2>Fancy a chat?</h2>
            <p>Drop me a message and let's grab a cuppa. I'm buying.</p>
            <a href="mailto:hello@adampool.com" class="button">Sounds like a plan</a>
        </div>

        <div class="bottom">
            <div class="social">
                <a href="https://www.linkedin.com/in/adam-pool-129b8125/" target="_BLANK" class="icon" aria-label="LinkedIn">
                    <svg width="24" height="24" viewBox="0 0 24 24">
                        <path d="M19 3A2 2 0 0 1 21 5V19A2 2 0 0 1 19 21H5A2 2 0 0 1 3 19V5A2 2 0 0 1 5 3H19M18.5 18.5V13.2A3.26 3.26 0 0 0 15.24 9.94C14.39 9.94 13.4 10.46 12.92 11.24V10.13H10.13V18.5H12.92V13.57C12.92 12.8 13.54 12.17 14.31 12.17A1.4 1.4 0 0 1 15.71 13.57V18.5H18.5M6.88 8.56A1.68 1.68 0 0 0 8.56 6.88C8.56 5.95 7.81 5.19 6.88 5.19A1.69 1.69 0 0 0 5.19 6.88C5.19 7.81 5.95 8.56 6.88 8.56M8.27 18.5V10.13H5.5V18.5H8.27Z" />
                    </svg>
                </a>
                <a href="https://twitter.com/AdamPoolUK" target="_BLANK" class="icon" aria-label="Twitter">
                    <svg width="24" height="24" viewBox="0 0 24 24">
                        <path d="M22.46,6C21.69,6.35 20.86,6.58 20,6.69C20.88,6.16 21.56,5.32 21.88,4.31C21.05,4.81 20.13,5.16 19.16,5.36C18.37,4.5 17.26,4 16,4C13.65,4 11.73,5.92 11.73,8.29C11.73,8.63 11.77,8.96 11.84,9.27C8.28,9.09 5.11,7.38 3,4.79C2.63,5.42 2.42,6.16 2.42,6.94C2.42,8.43 3.17,9.75 4.33,10.5C3.62,10.5 2.96,10.3 2.38,10C2.38,10 2.38,10 2.38,10.03C2.38,12.11 3.86,13.85 5.82,14.24C5.46,14.34 5.08,14.39 4.69,14.39C4.42,14.39 4.15,14.36 3.89,14.31C4.43,16 6,17.26 7.89,17.29C6.43,18.45 4.58,19.13 2.56,19.13C2.22,19.13 1.88,19.11 1.54,19.07C3.44,20.29 5.7,21 8.12,21C16,21 20.33,14.46 20.33,8.79C20.33,8.6 20.33,8.42 20.32,8.23C21.16,7.63 21.88,6.87 22.46,6Z" />
                    </svg>
                </a>
                <a href="https://www.instagram.com/adampooluk/" target="_BLANK" class="icon" aria-label="Instagram">
                    <svg width="24" height="24" viewBox="0 0 24 24">
                        <path d="M7.8,2H16.2C19.4,2 22,4.6 22,7.8V16.2A5.8,5.8 0 0,1 16.2,22H7.8C4.6,22 2,19.4 2,16.2V7.8A5.8,5.8 0 0,1 7.8,2M7.6,4A3.6,3.6 0 0,0 4,7.6V16.4C4,18.39 5.61,20 7.6,20H16.4A3.6,3.6 0 0,0 20,16.4V7.6C20,5.61 18.39,4 16.4,4H7.6M17.25,5.5A1.25,1.25 0 0,1 18.5,6.75A1.25,1.25 0 0,1 17.25,8A1.25,1.25 0 0,1 16,6.75A1.25,1.25 0 0,1 17.25,5.5M12,7A5,5 0 0,1 17,12A5,5 0 0,1 12,17A5,5 0 0,1 7,12A5,5 0 0,1 12,7M12,9A3,3 0 0,0 9,12A3,3 0 0,0 12,15A3,3 0 0,0 15,12A3,3 0 0,0 12,9Z" />
                    </svg>
                </a>
                <a href="https://www.facebook.com/apool6" target="_BLANK" class="icon" aria-label="Facebook">
                    <svg viewBox="0 0 24 24" width="24px" height="24px">
                        <path d="M19,3H5C3.895,3,3,3.895,3,5v14c0,1.105,0.895,2,2,2h7.621v-6.961h-2.343v-2.725h2.343V9.309 c0-2.324,1.421-3.591,3.495-3.591c0.699-0.002,1.397,0.034,2.092,0.105v2.43h-1.428c-1.13,0-1.35,0.534-1.35,1.322v1.735h2.7 l-0.351,2.725h-2.365V21H19c1.105,0,2-0.895,2-2V5C21,3.895,20.105,3,19,3z"/>
                    </svg>
                </a>
            </div>
            <span class="copyright">&copy; <?php echo date("Y"); ?> Adam Pool</span>
        </div>
    </div>
</section>

<?php get_footer(); ?>