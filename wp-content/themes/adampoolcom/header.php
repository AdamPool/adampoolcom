<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package adampoolcom
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Adam Pool: Web Developer, Designer and Problem Solver">

	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/site.webmanifest">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>
	<div id="page" class="site">
		<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e('Skip to content', 'adampoolcom'); ?></a>

		<header id="masthead" class="site-header">
			<div class="main-section">
				<nav id="site-navigation" class="main-navigation">
					<label>
						<input type="checkbox">
						<svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
							<circle cx="50" cy="50" r="30" />
							<path class="line--1" d="M0 40h62c13 0 6 28-4 18L35 35" />
							<path class="line--2" d="M0 50h70" />
							<path class="line--3" d="M0 60h62c13 0 6-28-4-18L35 65" />
						</svg>
					</label>
					<div class="outer-menu">
						<div>
							<?php
							wp_nav_menu(
								array(
									'theme_location' => 'menu-1',
									'menu_id'        => 'primary-menu',
								)
							);
							?>
						</div>
					</div>
				</nav><!-- #site-navigation -->
			</div>
		</header><!-- #masthead -->